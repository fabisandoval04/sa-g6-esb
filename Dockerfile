FROM node:10
# Crear el directorio de la app
WORKDIR /usr/src/esb
# instalar dependencias
COPY package*.json ./
COPY index.js ./
RUN npm install

# copiar el codigo
COPY . .
EXPOSE 5005
CMD ["node", "index.js"]