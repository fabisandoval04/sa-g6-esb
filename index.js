const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const fs = require('fs');
const axios = require('axios');
const jwt = require('jsonwebtoken');
var publicKEY = fs.readFileSync('./key/public.key', 'utf8');
/*
const PUBLIC_KEY = process.env.PUBLIC_KEY || `-----BEGIN PUBLIC KEY-----
MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgFtrLl2JH5B0BLlQFX0Il4tWTuA9
goiDXM95kQz+LUqaLXJEcKnvpvSJUqdh2QZfTjspzEunD9VccI1EOiFL4+ny+vVN
zzw80YVkpWekHa20iviaYP2i8SdZdIxQw5m5IJBgw8YrOGqIXRuXzTFAHnicEaM+
2VtBJbOfGEN/caSBAgMBAAE=
-----END PUBLIC KEY-----`;
*/
const PORT_ESB = process.env.PORT_ESB || "5005";

app.use(bodyParser());


app.listen(PORT_ESB, () =>
  console.log(`ESB escuchando en el puerto ${PORT_ESB}...`)
);


app.get("/", (req, res) => {
  res.json({
    text: `El servicio de esb corre en el puerto ${PORT_ESB}`
  });
});


app.post('/post/comunicacion', (req, res) => {

  if (validar_parametros(req.body.token, req.body.url, req.body.tipo, req.body.funcionSolicitada)) {
    var token = req.body.token;
    /*
    * Verifying token
    */
    var i = 'Mysoft corp';
    var s = 'some@user.com';
    var a = 'http://mysoftcorp.in';

    var verifyOptions = {
      issuer: i,
      subject: s,
      audience: a,
      expiresIn: '120h',
      algorithm: ['RS256']
    };

    try {
      var tokenVerificado = jwt.verify(token, publicKEY, verifyOptions);
      var fechaActual = Math.floor(Date.now() / 1000);
      var fechaExp = tokenVerificado.exp;

      //console.log(`fechaExp: ${fechaExp} - fechaActual: ${fechaActual}`);
      if (fechaExp - fechaActual > 0) {

        var scopes = tokenVerificado.scopes;
        var funcionSolicitada = req.body.funcionSolicitada;
        let scopeValido = 1;
        for (let i = 0; i < scopes.length; i++) {
          if (funcionSolicitada === scopes[i]) {
            scopeValido = 1;
            break;
          }
        }

        if (scopeValido == 1) {

          var url = req.body.url;
          var tipo = req.body.tipo.toUpperCase();
          var parametros = req.body.parametros;

          switch (tipo) {
             
            case "POST":
                console.log("hare un post");
              if (parametros === undefined) {
                res.send({ estado: 500, mensaje: 'Falta el parametro obligatorio : Parametros' });
              } else {
                axios.post(url, parametros)
                  .then(responsePost => {
                    res.send(responsePost.data);
                  })
                  .catch(function (error) {
                    res.send({ estado: 500, mensaje: 'Sin respuesta a la solicutd en la ruta ' + url });
                  });
              }
              break;
            case "GET":
              console.log("hare un get a " + url);
              axios.get(url)
                .then(responseGET => {
                  res.status(200).send(responseGET.data);
                })
                .catch(function (error) {
                  res.status(500).send({ estado: 500, mensaje: 'Sin respuesta a la solicutd en la ruta ' + url });
                });
              break;

            case "DELETE":
                console.log("hare un get a" + url);
              axios.delete(url)
                .then(responseDelete => {
                  res.status(200).send(responseDelete.data);
                })
                .catch(function (error) {
                  res.status(500).send({ estado: 500, mensaje: 'Sin respuesta a la solicutd en la ruta ' + url });
                });
              break;

            default:
              res.status(400).send({ estado: 400, mensaje: 'Tipo de Solicitud Invalida' });
              break;
          }
        }
        else {
          console.log("Funcion solicitada no esta dentro del scope");
          res.status(400).send({ estado: 400, mensaje: 'Funcion solicitada no esta dentro del scope' });
        }
      }
      else {
        console.log("token ya expirado");
        res.status(400).send({ estado: 400, mensaje: 'Token ya expirado' });
      }
    }
    catch (err) {
      console.log("token invalido");
      console.log(err);
      res.status(400).send({ estado: 400, mensaje: 'Token invalido' });
    }
  }
  else {
    console.log("parametros obligatorios");
    res.status(500).send({ estado: 500, mensaje: 'Faltan parametros obligatorios' });
  }

});


function validar_parametros(token, url, tipo, funcionSolicitada) {

  if (token === undefined || url === undefined || tipo === undefined || funcionSolicitada === undefined) {
    return false;
  }
  else {
    return true;
  }
}